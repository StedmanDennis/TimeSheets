'use strict'; //allows use of let for block scope

var mysql = require('mysql'); 
var express = require('express');
var http = require('http');//
var bodyParser = require('body-parser');//
var dateformat = require('dateformat');//
var path = require('path');
var app = express();

//app.use(bodyParser.urlencoded({ extended: true})); //
var urlParser = bodyParser.urlencoded({ extended: true});

//app.set('view engine', 'ejs'); //

app.use(express.static(__dirname));
app.use(bodyParser.json());
//used to redirect to main page

var connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
});

connection.connect(function(err) {
	if (!err){
		console.log("Connected to mysql");
	}else{
		console.log("Error: "+err);
	}
	
	connection.query("CREATE DATABASE IF NOT EXISTS timeSheetApp", function (err, result){
		if (!err){
			console.log("Created database");
		}else{
			console.log("Error: "+err);
		}
	});

	connection.query("USE timeSheetApp", function (err, result){
		if (!err){
			console.log("Connected to database");
		}else{
			console.log("Error: "+err);
		}
	});

	connection.query("CREATE TABLE IF NOT EXISTS login_details (userID int primary key auto_increment, password varchar(12) not null, credentials varchar(30) not null)", function (err, result){
		if (!err){
			console.log("Connected to login_details table created");
		}else{
			console.log("Error: "+err);
		}
	});


	connection.query("INSERT INTO `login_details`(`userID`, `password`, `credentials`) VALUES (1, 'stedman11', 'sted@email.com')", function (err, result){
		if (!err){
			console.log("Inserted login Details");
		}else{
			console.log("Error: "+err);
		}
	});

	connection.query("CREATE TABLE IF NOT EXISTS user_details (userID int primary key, userFName varchar(20) not null, userLName varchar(20) not null, Address tinytext, FOREIGN KEY (userID) REFERENCES login_details(userID))", function (err, result){
		if (!err){
			console.log("Connected to user_details table created");
		}else{
			console.log("Error: "+err);
		}
	});

	connection.query("INSERT INTO `user_details`(`userID`, `userFName`, `userLName`, `Address`) VALUES (1,'Stedman','Dennis','18 somewhere, in a country, on a planet, in the universe')", function (err, result){
		if (!err){
			console.log("Inserted user Details");
		}else{
			console.log("Error: "+err);
		}
	});

	connection.query("CREATE TABLE IF NOT EXISTS client_details (clientID int primary key auto_increment, manager varchar(30) not null, position varchar(20) not null)", function (err, result){
		if (!err){
			console.log("Connected to client_details table created");
		}else{
			console.log("Error: "+err);
		}
	});

	connection.query("INSERT INTO `client_details`(`clientID`, `manager`, `position`) VALUES (1,'Dennis','Human Resource Manager')", function (err, result){
		if (!err){
			console.log("Inserted client Details");
		}else{
			console.log("Error: "+err);
		}
	});

	connection.query("CREATE TABLE IF NOT EXISTS tasks (taskID int primary key auto_increment, userID int, clientID int, title varchar(30), FOREIGN KEY (userID) REFERENCES login_details(userID), FOREIGN KEY (clientID) REFERENCES client_details(clientID))", function (err, result){
		if (!err){
			console.log("Connected to tasks table created");
		}else{
			console.log("Error: "+err);
		}
	});

	connection.query("INSERT INTO `tasks` (`taskID`, `userID`, `clientID`, `title`) VALUES ('1', '1', '1', 'Timesheet App');", function (err, result){
		if (!err){
			console.log("Inserted tasks");
		}else{
			console.log("Error: "+err);
		}
	});

	connection.query("CREATE TABLE IF NOT EXISTS time_periods (periodID int primary key auto_increment, userID int, taskID int, date date not null, startTime time not null, endTime time not null, comment tinytext,FOREIGN KEY (userID) REFERENCES login_details(userID), FOREIGN KEY (taskID) REFERENCES tasks(taskID))", function (err, result){
		if (!err){
			console.log("Connected to time_periods table created");
		}else{
			console.log("Error: "+err);
		}
	});

	connection.query("INSERT INTO `time_periods` (`periodID`, `userID`, `taskID`, `date`, `startTime`, `endTime`, `comment`) VALUES ('1', '1', '1', '2018-07-10', '22:00:00', '23:00:00', 'Downloaded XAMPP and node.js for Project')", function (err, result){
		if (!err){
			console.log("Inserted time periods");
		}else{
			console.log("Error: "+err);
		}
	});
});

//CRUD Logic
app.post('/login', urlParser, function(req, res){
	var receivedUser = req.body.userName;
	var receivedPass = req.body.password;
	var sendBack = 	{
						success: false,
						userDetails: []
					};
	var sql = "SELECT * FROM login_details WHERE credentials = ? AND password = ?";
	var sql2 ="SELECT * FROM user_details WHERE userID = ?"; 
	connection.query(sql,[receivedUser, receivedPass], function (err, result){
		if (!err){
			if (result.length != 0){
				connection.query(sql2,[result[0].userID], function (err, result){
					if(!err){
						sendBack = 	{
										success: true,
										userDetails: result
									};
						res.send(sendBack); //send successful login response
					}else{
						console.log(err);
					}
				});
			}else{
				res.send(sendBack); //send fail to login response
			}
		}else{
			console.log(err);
		}
	})
});

/*
app.post('/tasks', urlParser, function(req, res){
	var receivedUser = req.body.userID;
	var sendBack;
	var sql = "SELECT * FROM tasks WHERE userID = ?";
	connection.query(sql,[receivedUser], function (err, result){
		if (!err){
			sendBack = result;
			res.send(sendBack);
		}else{
			console.log(err);
		}
	});
});
*/

app.post('/tasks', urlParser, function(req, res){
	var receivedUser = req.body.userID;
	var sendBack = 	{
						tasks: [],
					};
	var sql = 	`	SELECT a.taskID, a.title, b.clientID, b.manager, b.position
					FROM tasks AS a
					JOIN client_details AS b
					ON a.clientID = b.clientID
					WHERE a.userID = ?
				`;
	var sql2 = 	`	SELECT periodID, DATE_FORMAT(date, '%Y/%m/%d') AS date, startTime, endTime, comment
					FROM time_periods
					WHERE taskID = ?
				`;
	connection.query(sql, [receivedUser], function (err, result){
		if (!err){
			sendBack.tasks = result;
			//console.log(sendBack);
			if (sendBack.tasks.length > 0){//if the user has at least 1 task
				for (let taskIndex = 0; taskIndex <= sendBack.tasks.length - 1; taskIndex++){//https://stackoverflow.com/questions/16068290/javascript-nodejs-mysql-with-queries-in-a-loop/36198496#36198496?newreg=164bd20d856b489ea36434840f23b80d
					connection.query(sql2, [sendBack.tasks[taskIndex].taskID], function(err, result){
						if (!err){
							sendBack.tasks[taskIndex].timePeriods = result;
							console.log(sendBack.tasks[taskIndex].timePeriods);
							console.log(taskIndex);
							if (taskIndex == sendBack.tasks.length - 1){
								console.log(sendBack.tasks.length);
								console.log('sent');
								res.send(sendBack);
							}
						}else{
							console.log(err);
						}
					});
				}
			//res.send(sendBack) //will run BEFORE the loop starts and send an incomplete response
			}else{//if the user has no tasks
				res.send(sendBack);
			}
			console.log('after loop')
			//res.send(sendBack);
		}else{
			console.log(err);
		}
	});
})

app.get('/', function(req, res){
	res.sendFile(path.join(__dirname + '/timeSheet.html'));
});

app.listen(1337, function(){
	console.log("Sever start on port: 1337");
});