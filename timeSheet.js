Vue.component('login', {
	template: 	`
					<div class='login basicBorder'>
						<label for='userName'>User Name: </label>
						<input type='text' name='userName' v-model='userName'>
						<label for='userName'>Password: </label>
						<input type='password' name='password' v-model='password'>
						<button v-on:click='loadData'>login</button>
					</div>
				`,
	data: function(){
		return {
			userName: 'sted@email.com',
			password: 'stedman11',
		}
	},
	methods: {
		loadData: function(){
			$.ajax({
				url: '/login',
				method: 'POST',
				data: 	JSON.stringify({
							userName: this.userName,
							password: this.password
						}),
				contentType: 'application/json',
				success: function(response){
					if (response.success){
						eventBus.$emit('successfulLogin', response.userDetails[0]);
						console.log("login success");
						console.log(response.userDetails[0]);
					}else{
						console.log("Possible Incorrect login details");
					}
				}
			})
		}
	}
})

Vue.component('task', {
	props: ['task','selectedTask'],
	template: 	`
				<div class='task basicBorder' v-bind:class="{selectedTask: selectedTask == task}">
					<div class='taskID' v-on:click='clicked'>{{task.taskID}}</div>
				</div>
				`,
	methods: {
		clicked: function(){
			eventBus.$emit('selectedTask', this.task);	
		}
	}
})

Vue.component('add-task-button', {
	template: 	`
				<div class='addTaskButton' v-on:click='changeToAddMode'>
				Add a Task
				</div>
				`,
	methods: {
		changeToAddMode: function(){
			eventBus.$emit('addModeOn');
		}
	}
})

Vue.component('time-period', {
	props: ['period'],
	template: 	`
				<div class='timePeriod basicBorder'>
					<div class='dateContainer'>
						Date: {{period.date}}
					</div>
					<div class='timeContainerS'>
						From: {{period.startTime}}
					</div>
					<div class='timeContainerE'>
						To: {{period.endTime}}
					</div>
					<div class='comment'>
						Comment: {{period.comment}}
					</div>
				</div>
				`,
})

Vue.component('task-view',{
	props: ['selectedTask'],
	template: 	`
				<div class='taskView basicBorder'>
					<div class='viewTitle'>{{selectedTask.title}}</div>
					<div class='viewManager'>{{selectedTask.manager}}</div>
					<div class='viewPosition'>{{selectedTask.position}}</div>
					<div class='periodList'>
						<time-period v-for='period in selectedTask.timePeriods' v-bind:period='period'></time-period>
					</div>
				</div>
				`,
})

Vue.component('task-add', {
	template: 	`
				<div class='taskAdd basicBorder'>
					<div class='addTitle'>
						<label for='taskTitle'>Task Title:</label>
						<input type='text' name='taskTitle' placeholder='Title' v-model='title'>
					</div>
					<div class='addManager'>
						<label for='taskManager'>Task Manager:</label>
						<input type='text' name='taskManager' placeholder='Manager' v-model='manager'>
					</div>
					<div class='addPeriodList basicBorder'>
						<add-period v-for='period in periods' v-bind:period=period></add-period>
					</div>
					<div class='addTaskButton' v-on:click='addPeriod'>
						Add a Period
					</div>
				</div>
				`,
	data: function(){
		return{
			title: '',
			manager: '',
			periods: []
		}
	},
	methods: {
		addPeriod: function(){
			var newPeriod = {
				date: "",
				start: "",
				end: "",
				timeGap: 0	
			};
			this.periods.push(newPeriod);
		}
	}
})

Vue.component('add-period', {
	props: ['period'],
	template: 	`
				<div class='addTimePeriod basicBorder'>
					<div class='addPeriodDate'>
						<label for='addPeriodDate'>Date</label>
						<input type='date' name='addPeriodDate' v-model='period.date'>
					</div>
					<div class='addPeriodStart'>
						<label for='addPeriodStart'>Start Time</label>
						<input type='time' name='addPeriodStart' v-model='period.start'>
					</div>
					<div class='addPeriodEnd'>
						<label for='addPeriodEnd'>End Time</label>
						<input type='time' name='addPeriodEnd' v-model='period.end'>
					</div>
					<div>
						Worked: {{gapHours}} Hours {{gapMinutes}} Minutes ({{(gapHours+(gapMinutes/60)).toFixed(2)}} hours)
					</div>
				</div>
				`,
	computed:{
		gapHours: function(){
			var endTime = new Date(this.period.date+'T'+this.period.end+'Z');
			var startTime = new Date(this.period.date+'T'+this.period.start+'Z');
			var result = new Date (endTime - startTime);
			this.period.timeGap = result.getUTCHours();
			return result.getUTCHours();
		},
		gapMinutes: function(){
			var endTime = new Date(this.period.date+'T'+this.period.end+'Z');
			var startTime = new Date(this.period.date+'T'+this.period.start+'Z');
			var result = new Date (endTime - startTime);
			return result.getUTCMinutes();
		}
	}
})

Vue.component('user-dashboard', {
	props: ['userDetails'],
	template: 	`	
				<div class='dashboard basicBorder'>
					<div class='clientDetails basicBorder'>
						<div class='userName'>{{userDetails.userFName}} {{userDetails.userLName}}</div>
						<div class='address'>{{userDetails.Address}}</div>
					</div>
					<div class='taskCenter basicBorder'>
						<div class='taskList basicBorder'>
							<task v-for='task in userTasks' v-bind:task='task' v-bind:selectedTask='selectedTask'></task>
							<add-task-button></add-task-button>
						</div>
						<div v-if='selectedTask == null && !addMode'>No Task Selected</div>
						<task-view v-if='selectedTask != null' v-bind:selectedTask='selectedTask'></task-view>
						<keep-alive>
							<task-add v-if='addMode'></task-add>
						</keep-alive>
					</div>
				</div>
				`,
	data: function(){
		return{
			userTasks: [],
			selectedTask: null,
			addMode: false
		}
	},
	methods:{
		getTasks: function(){
			var vm = this;
			var userID = vm.userDetails.userID;
			$.ajax({
				url: '/tasks',
				method: 'POST',
				data: 	JSON.stringify({
							userID: userID
						}),
				contentType: 'application/json',
				success: function(response){
					vm.userTasks = response.tasks;
				}
			})
		},
	},
	created: function(){
		eventBus.$on('selectedTask', (payload) => {
			this.addMode = false;
			this.selectedTask = payload;
		});
		eventBus.$on('addModeOn', () => {
			this.addMode = true;
			this.selectedTask = null;
		});
	},
	beforeDestroy: function(){
		eventBus.$off('selectedTask');
		eventBus.$off('addMode');
	},
	mounted: function(){
		this.getTasks();
	}
})

const eventBus = new Vue({}) //global event bus

new Vue({
	el: 		'#vue-app',
	template: 	`
				<login v-if='loggedIn == null'></login>
				<user-dashboard v-else v-bind:userDetails='loggedIn'></user-dashboard>
				`,
	data: {
		loggedIn: null,

	},
	created: function(){
		eventBus.$on('successfulLogin', payload => this.loggedIn = payload);
	},
	beforeDestroy: function(){
		eventBus.$off('successfulLogin');
	}
})